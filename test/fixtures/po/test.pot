#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2016-06-20 07:27+0000\n"
"Plural-Forms: \n"

#: test/fixtures/foo.json:bar
#: test/fixtures/foo.json:fuga
msgid "foo"
msgstr ""

#: test/fixtures/foo.json:baz##hoge
msgid "str"
msgstr ""

#: test/fixtures/foo.json:baz##abc##gosh
msgid "xxx"
msgstr ""

#: test/fixtures/foo.json:baz##def
msgid "fixme"
msgstr ""