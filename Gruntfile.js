/*
 * grunt-gettext-json
 * https://github.com/tagoh/grunt-gettext-json
 *
 * Copyright (c) 2016 Akira TAGOH
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: ['Gruntfile.js', 'tasks/*.js'],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Before generating any new files, remove any previously-created files.
    clean: {
      tests: ['tmp']
    },

    // Configuration to be run (and then tested).
    'gettext-json': {
      json2pot: {
        options: {
        },
        files: [
          {src: 'test/fixtures/*.json', dest: 'test/fixtures/po/test.pot'}
        ]
      },
      po2json: {
        options: {
          srcdir: 'test/fixtures'
        },
        files: [
          {src: 'test/fixtures/po/*.po', dest: 'test/fixtures/lang/__name__.json.__lang__'}
        ]
      },
      json2pot_targetkeysArray: {
        options: {
          translatableKeys: ['bar', 'def']
        },
        files: [
          {src: 'test/fixtures/*.json', dest: 'test/fixtures/po/test_targetArray.pot'}
        ]
      }
    }
  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-readme');

  grunt.registerTask('test', ['clean', 'gettext-json']);
  // By default, lint and run all tests.
  grunt.registerTask('default', ['jshint', 'repos', 'readme']);
};
