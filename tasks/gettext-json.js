/*
 * grunt-gettext-json
 * https://bitbucket.org/tagoh/grunt-gettext-json
 *
 * Copyright (c) 2016 Akira TAGOH
 * Licensed under the MIT license.
 */

'use strict';

var gj = require('../libs/gettext-json.js');
var path = require('path');

module.exports = function(grunt) {

  // Please see the grunt documentation for more information regarding task
  // creation: https://github.com/gruntjs/grunt/blob/devel/docs/toc.md

  grunt.registerMultiTask('gettext-json', 'Compile PO from JSON/Generate localized JSON from PO', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({});
    var self = this;
    var target = this.target.split('_')[0];

    if (target === 'json2pot') {
      this.files.forEach(function(f) {
        var jsonfiles = grunt.file.expand({nonull: true}, f.src).filter(function(ff){return grunt.file.exists(ff);});
        var po = gj.json2pot(jsonfiles, options);
        grunt.file.write(f.dest, po);
        grunt.log.writeln(jsonfiles.join(',') + ' -> ' + f.dest);
      });
    } else if (target === 'po2json') {
      this.files.forEach(function(f) {
        var pofiles = grunt.file.expand({nonull: true}, f.src).filter(function(ff){return grunt.file.exists(ff);});
        pofiles.forEach(function(pofile) {
          try {
            gj.po2json(pofile, options, function(fn, lang, data) {
              var name = path.basename(fn, path.extname(fn));
              var dir = path.dirname(fn);
              var dest = f.dest.replace(/__path__/g, dir).replace(/__name__/g, name).replace(/__lang__/g, lang);
              grunt.file.write(dest, JSON.stringify(data));
              grunt.log.writeln(pofile + ' -> ' + dest);
            });
          } catch (e) {
            if (options.ignoreError) {
              grunt.log.writeln(e);
            } else {
              throw e;
            }
          }
        });
      });
    } else {
      grunt.log.error('Invalid target: ' + this.target);
      return false;
    }
  });

};
