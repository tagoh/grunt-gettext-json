'use strict';

var fs = require('fs');
var gettextParser = require('gettext-parser');
var util = require('util');
var path = require('path');

var traverseObject = function(obj, _path, callback) {
  for (var i in obj) {
    var p = _path.length > 0 ? _path + '##' + i : i;
    if (typeof (obj[i]) == "string" && obj[i].length > 0) {
      callback.apply(this, [p, i, obj[i]]);
    } else if (obj[i] !== null && typeof (obj[i]) == 'object') {
      traverseObject(obj[i], p, callback);
    }
  }
};

exports.json2pot = function(files, options) {
  var translations = {'': {'': {msgid: '', comments: {flag: 'fuzzy'}}}};
  var keys, translatableKeys = options.translatableKeys instanceof Array ? {'': options.translatableKeys} : options.translatableKeys;

  // XXX: no msgctxt support so far
  files.forEach(function(fn) {
    var json = JSON.parse(fs.readFileSync(fn, 'utf-8'));
    traverseObject(json, '', function(p, k, v) {
      if (translatableKeys) {
        if (translatableKeys[fn])
          keys = translatableKeys[fn];
        else
          keys = translatableKeys[''];
      }
      if (keys === undefined || (keys instanceof Array && keys.indexOf(k) > -1)) {
        if (translations[''][v] == undefined) {
          var msg = {msgctxt: '', msgstr: '', msgid: v, comments: {reference: fn + ':' + p}};
          translations[''][v] = msg;
        } else {
          var msg = translations[''][v];
          if (msg.comments.reference.length > 0)
            msg.comments.reference += '\n';
          msg.comments.reference += fn + ':' + p;
        }
      }
    });
  });

  var data = {charset: 'utf-8',
              headers: {
                'project-id-version': 'PACKAGE VERSION',
                'language-team': 'LANGUAGE <LL@li.org>',
                'po-revision-date': 'YEAR-MO-DA HO:MI+ZONE',
                'language': '',
                'mime-version': '1.0',
                'content-type': 'text/plain; charset=UTF-8',
                'content-transfer-encoding': '8bit',
                'pot-creation-date': new Date().toISOString().replace('T', ' ').replace(/:\d{2}.\d{3}Z/, '+0000'),
                'plural-forms': ''
              },
              translations: translations
             };

  return gettextParser.po.compile(data);
};

var setValue = function(obj, fn, _path, pos, value) {
  if (_path === undefined)
    throw new Error('Invalid path');
  // No objects to localize. the PO might be outdated. simply ignoring.
  if (obj === undefined) {
    console.warn(util.format('%s:%s: No paths in JSON to be localized. PO might be outdated', fn, _path.join('->')));
    return;
  }
  if (_path.length > (pos + 1)) {
    setValue(obj[_path[pos]], fn, _path, pos + 1, value);
  } else {
    if (obj[_path[pos]]) {
      obj[_path[pos]] = value;
    } else {
      console.warn(util.format('%s:%s: No paths in JSON to be localized. PO might be outdated', fn, _path.join('->')));
    }
  }
};

exports.po2json = function(file, opts, callback) {
  if (typeof (opts) === 'function') {
    callback = opts;
    opts = {};
  }
  var options = opts ? opts : {};
  var contents = fs.readFileSync(file, 'utf-8');
  var data = gettextParser.po.parse(contents);
  var json = {};
  var lang;
  var f;

  if (data && data.headers && data.headers.language) {
    lang = data.headers.language;
  } else if ((f = path.basename(file).match(/^([a-zA-Z]{2,3}(?:_[a-zA-Z]{2,3})?)\.po$/))) {
    lang = f[1];
  } else {
    throw new Error('Unable to guess the target language');
  }
  for (var i in data.translations['']) {
    var msg = data.translations[''][i];

    if ((msg.comments.flag == undefined || !msg.comments.flag.match(/fuzzy/)) && msg.comments.reference) {
      var refs = msg.comments.reference.split('\n');
      for (var j in refs) {
        var x = refs[j].split(':');
        var fn = x[0];
        var _path = x[1].split('##');
        if (options.srcdir) {
          fn = path.join(options.srcdir, path.basename(fn));
        }
        if (json[fn] === undefined)
          json[fn] = JSON.parse(fs.readFileSync(fn, 'utf-8'));
        if (msg.msgstr[0])
          setValue(json[fn], fn, _path, 0, msg.msgstr[0]);
      }
    }
  }
  for (var i in json) {
    callback.apply(this, [i, lang, json[i]]);
  }
};
