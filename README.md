# grunt-gettext-json [![NPM version](https://badge.fury.io/js/grunt-gettext-json.png)](http://badge.fury.io/js/grunt-gettext-json)

> A Grunt task to deal with the gettext format file in JSON

## Getting Started
_If you haven't used [grunt][] before, be sure to check out the [Getting Started][] guide._

From the same directory as your project's [Gruntfile][Getting Started] and [package.json][], install this plugin with the following command:

```bash
npm install grunt-gettext-json --save-dev
```

Once that's done, add this line to your project's Gruntfile:

```js
grunt.loadNpmTasks('grunt-gettext-json');
```

If the plugin has been installed correctly, running `grunt --help` at the command line should list the newly-installed plugin's task or tasks. In addition, the plugin should be listed in package.json as a `devDependency`, which ensures that it will be installed whenever the `npm install` command is run.

[grunt]: http://gruntjs.com/
[Getting Started]: https://github.com/gruntjs/grunt/blob/devel/docs/getting_started.md
[package.json]: https://npmjs.org/doc/json.html

## The "gettext-json" task

### Overview
In your project's Gruntfile, add a section named `gettext-json` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  'gettext-json': {
    your_target: {
      // Target-specific file lists and/or options go here.
    },
  },
})
```

### Targets

This plugin behaves differently against the target. the available target is:

#### json2pot

This target will generates a POT file against JSON files.

#### po2json

This target will generates a localized JSON files against PO files. if containing non-translated entries, the original strings will be simply used.

### Options

#### options.srcddir
Type: `String`
Default value: `undefined`

Only takes effect on `po2json` target. the place to the source directory of the original JSON. if not specified, the full path of the reference comment in PO will be used.

#### options.translatableKeys
Type: `Array` or `Object`
Default value: `undefined`

Only takes effect on `json2pot` target. matched values in `Array` will be exported into POT. if you want to apply this for a certain file, put the filename as a key into Object.

#### options.ignoreError
Type: `Boolean`
Default value: `undefined`

Ignore an error on processing tasks.

### Usage Examples

#### Generating POT file from JSON
In this example, all of strings as a value in 'test/fixtures/*.json' will be put into 'test/fixtures/po/test.pot' as msgids.

```js
grunt.initConfig({
  'gettext-json': {
    json2pot: {
      files: [
        {src: 'test/fixtures/*.json', dest: 'test/fixtures/po/test.pot'}
      ]
    }
  },
})
```

#### Generating localized JSON from POs
In this example, translated entries in `test/fixtures/po/*.po` will replaces the original one and stored as `test/fixtures/lang/__name__.json.__lang__`. if `srcdir` as options is specified, it will be refered no matter what POs has as a reference. `__name__` in `dest` will be replaced to the original basename. `__lang__` will be the language in PO.

```js
grunt.initConfig({
  'gettext-json': {
    po2json: {
      options: {
        srcdir: 'test/fixtures'
      },
      files: [
        {src: 'test/fixtures/po/*.po', dest: 'test/fixtures/lang/__name__.json.__lang__'}
      ]
    }
  },
})
```

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [grunt][].


## License
Copyright (c) 2016 Akira TAGOH
Licensed under the [MIT license](LICENSE-MIT).

***

Project created by [Akira TAGOH](https://bitbucket.org/tagoh).

_This file was generated on Fri Jun 17 2016 17:23:07._
